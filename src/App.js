import React from "react";

export default class Secret extends React.Component {
  constructor(props) {
    super(props);
    this.state = { name: "Top Secret!" };
    this.onButtonClick = this.onButtonClick.bind(this);
  }

  onButtonClick() {
    this.setState(() => ({
      name: "Paul 'I write code!' Grenyer!"
    }));
  }

  render() {
    return (
      <div>
        <h1>My name is {this.state.name}</h1>
        <button onClick={this.onButtonClick}>Reveal the secret!</button>
      </div>
    );
  }
}
